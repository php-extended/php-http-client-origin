<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-origin library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use InvalidArgumentException;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Stringable;

/**
 * OriginClient class file.
 * 
 * This class is an implementation of a client which adds origin headers of
 * incoming requests.
 * 
 * @author Anastaszor
 */
class OriginClient implements ClientInterface, Stringable
{
	
	/**
	 * The inner client.
	 * 
	 * @var ClientInterface
	 */
	protected ClientInterface $_client;
	
	/**
	 * Builds a new DoNotTrackClient with the given inner client.
	 * 
	 * @param ClientInterface $client
	 */
	public function __construct(ClientInterface $client)
	{
		$this->_client = $client;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Client\ClientInterface::sendRequest()
	 */
	public function sendRequest(RequestInterface $request) : ResponseInterface
	{
		if(!$request->hasHeader('Origin'))
		{
			$host = $request->getUri()->getHost();
			
			if('http' === $request->getUri()->getScheme() && null !== $request->getUri()->getPort() && 80 !== $request->getUri()->getPort())
			{
				$host .= ':'.((string) $request->getUri()->getPort());
			}
			
			if('https' === $request->getUri()->getScheme() && null !== $request->getUri()->getPort() && 443 !== $request->getUri()->getPort())
			{
				$host .= ':'.((string) $request->getUri()->getPort());
			}
			
			try
			{
				$request = $request->withHeader('Origin', $request->getUri()->getScheme().'://'.$host);
			}
			// @codeCoverageIgnoreStart
			catch(InvalidArgumentException $e)
			// @codeCoverageIgnoreEnd
			{
				// nothing to do
			}
		}
		
		return $this->_client->sendRequest($request);
	}
	
}
