<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-origin library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Domain\Domain;
use PhpExtended\HttpClient\OriginClient;
use PhpExtended\HttpMessage\Request;
use PhpExtended\HttpMessage\Response;
use PhpExtended\HttpMessage\Uri;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class OriginClientClient implements ClientInterface
{
	
	public $request;
	
	public function sendRequest(RequestInterface $request) : ResponseInterface
	{
		$this->request = $request;
		
		return new Response();
	}
	
}

/**
 * OriginClientTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpClient\OriginClient
 *
 * @internal
 *
 * @small
 */
class OriginClientTest extends TestCase
{
	
	/**
	 * The client to help.
	 *
	 * @var OriginClientClient
	 */
	protected OriginClientClient $_client;
	
	/**
	 * The object to test.
	 * 
	 * @var OriginClient
	 */
	protected OriginClient $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testAddHeaderHttp() : void
	{
		$request = new Request();
		$request = $request->withUri(new Uri('http', null, new Domain(['domain', 'com'])));
		$response = $this->_object->sendRequest($request);
		$this->assertInstanceOf(ResponseInterface::class, $response);
		$this->assertEquals('http://domain.com', $this->_client->request->getHeaderLine('Origin'));
	}
	
	public function testAddHeaderHttpPort() : void
	{
		$request = new Request();
		$request = $request->withUri(new Uri('http', null, new Domain(['domain', 'com']), 8080));
		$response = $this->_object->sendRequest($request);
		$this->assertInstanceOf(ResponseInterface::class, $response);
		$this->assertEquals('http://domain.com:8080', $this->_client->request->getHeaderLine('Origin'));
	}
	
	public function testAddHeaderHttps() : void
	{
		$request = new Request();
		$request = $request->withUri(new Uri('https', null, new Domain(['domain', 'com'])));
		$response = $this->_object->sendRequest($request);
		$this->assertInstanceOf(ResponseInterface::class, $response);
		$this->assertEquals('https://domain.com', $this->_client->request->getHeaderLine('Origin'));
	}
	
	public function testAddHeaderHttpsPort() : void
	{
		$request = new Request();
		$request = $request->withUri(new Uri('https', null, new Domain(['domain', 'com']), 8081));
		$response = $this->_object->sendRequest($request);
		$this->assertInstanceOf(ResponseInterface::class, $response);
		$this->assertEquals('https://domain.com:8081', $this->_client->request->getHeaderLine('Origin'));
	}
	
	public function testDoNothing() : void
	{
		$request = new Request();
		$request = $request->withUri(new Uri('http', null, new Domain(['domain', 'com'])));
		$request = $request->withAddedHeader('Origin', 'https://foo.bar');
		$response = $this->_object->sendRequest($request);
		$this->assertInstanceOf(ResponseInterface::class, $response);
		$this->assertEquals('https://foo.bar', $this->_client->request->getHeaderLine('Origin'));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_client = new OriginClientClient();
		
		$this->_object = new OriginClient($this->_client);
	}
	
}
